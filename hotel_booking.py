import tkinter as tk
from tkinter import *
import sys

def total_bill():
    global room_rent
    room_rent = 0
    if room_rent_select.get() == 1:
        room_rent = 200*int(enter_no_days.get())
    elif room_select.get() == 2:
         room_rent = 300*int(enter_no_of_days.get())
    elif room_select.get() == 3:
         room_rent = 500*int(enter_no_days.get())
    elif room_select.get() == 4:
         room_rent = 700*int(enter_no_days.get())
    
    global food_bill
    food_bill = 0
    if clicked_1.get() == "Dosa":
        food_bill += 7
    elif clicked_1.get() == "Bread omlet":
        food_bill += 8
    elif clicked_1.get() == "Pancake":
        food_bill += 9
    if clicked_2.get() == "chicken Biriyani":
         food_bill += 20
    elif clicked_2.get() == "Mexican brunch":
         food_bill += 18
    elif clicked_2.get() == "Italian Bowl":
         food_bill += 15
    if clicked_3.get() == "Dal Makhani":
         food_bill += 8
    elif clicked_3.get() == "Chinese Bowl":
         food_bill += 12
    elif clicked_3.get() == "Rice Bowl":
         food_bill += 10
    else:
         food_bill = 0
    
    global totl_bil
    global totl_w_tax
    totl_char = room_rent + food_bill
    totl_w_tax = 0.1 * totl_char
    totl_bil = totl_char + totl_w_tax
    bill_lbl["text"] = f"$ {(totl_bil) } \nTotal Bill"
    
def print_booking(file=None):
    total_bill()
    print("Booking Status\n", file=file)
    print(f"Customer Name: {enter_first.get()} {enter_last.get()}", file=file)
    print(f"Check in Date: {enter_in.get()} {enter_out.get()}\n", file=file)
    print("Number of beds", file=file)
    print("Number of nights\n", file=file)
    print("Desired Food Selected\n", file=file)
    print(f"Room cost: $ {room_rent}", file=file)
    print(f"Food Cost: $ {food_bill}", file=file)
    print(f"Taxes: $ {totl_w_tax}", file=file)
    print(f"Total Bill: $ {totl_bil}", file=file)

def place_booking():
        original_stdout = sys.stdout 
        if total_bill == 0:
            raise RuntimeError("Booking not complete")
        else:
            with open(f'Hotel Booking.txt', 'w') as out_file:
                sys.stdout = out_file
                print_booking(file=out_file)
                sys.stdout = original_stdout

def reset_booking():
    enter_first.delete(0, END)
    enter_last.delete(0, END)
    enter_in.delete(0, END)
    enter_out.delete(0, END)
    enter_no_days.delete(0, END)
    room_select.set(None)
    clicked_1.set(breakfast_options[0])
    clicked_2.set(lunch_options[0])
    clicked_3.set(dinner_options[0])


window = tk.Tk()
window.title("Hotel Bello Online Booking")
window.columnconfigure([0,1,2], minsize=300, weight=1)
window.rowconfigure([0,1,2], minsize=200, weight=1)

frame_e = tk.Frame(master=window)

cust_det = tk.Label(master=frame_e, text="Customer Details").grid(row=0, column=0, sticky="w")

enter_first = tk.Entry(master=frame_e, width=10)
enter_first.grid(row=1, column=0, sticky="e")
first_lbl = tk.Label(master=frame_e, text="First Name").grid(row=1, column=1, sticky="w")

enter_last = tk.Entry(master=frame_e, width=10)
enter_last.grid(row=1, column=2, sticky="e")
last_lbl = tk.Label(master=frame_e, text="Last Name").grid(row=1, column=3, sticky="w")

enter_in = tk.Entry(master=frame_e, width=10)
enter_in.grid(row=2, column=0, sticky="e")
in_lbl = tk.Label(master=frame_e, text="Check in Date").grid(row=2, column=1, sticky="w")

enter_out = tk.Entry(master=frame_e, width=10)
enter_out.grid(row=2, column=2, sticky="e")
out_lbl = tk.Label(master=frame_e, text="Check Out Date").grid(row=2, column=3, sticky="w")

empty_spcae = tk.Label(master=frame_e, text="").grid(row=3, column=0, sticky="w")

room_lbl = tk.Label(master=frame_e, text="Select No. of Bed").grid(row=4, column=0, sticky="w")
room_select = tk.IntVar()
bed_1 = tk.Radiobutton(master=frame_e, text="1 Bed", variable=room_select, value=1)
bed_1.grid(row=4, column=1, sticky="we")
bed_2 = tk.Radiobutton(master=frame_e, text="2 Bed", variable=room_select, value=2)
bed_2.grid(row=4, column=2, sticky="we")
bed_3 = tk.Radiobutton(master=frame_e, text="3 Bed", variable=room_select, value=3)
bed_3.grid(row=4, column=3, sticky="we")
bed_4 = tk.Radiobutton(master=frame_e, text="4 Bed", variable=room_select, value=4)
bed_4.grid(row=4, column=4, sticky="we")

enter_no_days = tk.Entry(master=frame_e, width=10)
enter_no_days.grid(row=5, column=0, sticky="e")
no_day_lbl = tk.Label(master=frame_e, text="ENter No. of Nights To Stay").grid(row=5, column=1, sticky="w")

empty_space = tk.Label(master=frame_e, text="").grid(row=6, column=0, sticky="w")

food_lbl = tk.Label(master=frame_e, text="Add your desired food").grid(row=7, column=0, sticky="w")
breakfast_options = [None, "Dosa", "Bread Omlet", "Pancake"]
clicked_1 = tk.StringVar()
clicked_1.set(breakfast_options[0])
breakfast_lbl = tk.Label(master=frame_e, text="Select Breakfast").grid(row=8, column=0, sticky="w")
breakfast_dropdown = tk.OptionMenu(frame_e, clicked_1, *breakfast_options)
breakfast_dropdown.grid(row=9, column=0, sticky="we")

lunch_options = [None, "Chicken Biryani", "Mexican Brunch", "Italian Bowl"]
clicked_2 = tk.StringVar()
clicked_2.set(lunch_options[0])
lunch_lbl = tk.Label(master=frame_e, text="Select Lunch").grid(row=8, column=1, sticky="w")
lunch_dropdown = tk.OptionMenu(frame_e, clicked_2, *lunch_options)
lunch_dropdown.grid(row=9, column=1, sticky="we")

dinner_options = [None, "Dal Makhani", "Chinese Bowl", "Rice Bowl"]
clicked_3 = tk.StringVar()
clicked_3.set(dinner_options[0])
dinner_lbl = tk.Label(master=frame_e, text="Select Dinner").grid(row=8, column=2, sticky="w")
dinner_dropdown = tk.OptionMenu(frame_e, clicked_3, *dinner_options)
dinner_dropdown.grid(row=9, column=2, sticky="we")

empty_space = tk.Label(master=frame_e, text="").grid(row=11, coloumn=0, sticky="w")

btn_bill = tk.Button(master=frame_e, text="OK", command=hotelbook.total_bill).grid(row=12, column=1, sticky="we")
bill_lbl = tk.Label(master=frame_e, text="Total Bill").grid(row=12, column=0, sticky="we")
btn_bill = tk.Button(master=frame_e, text="OK", command=total_bill)
btn_bill.grid(row=11, column=1, sticky="we")
bill_lbl = tk.Label(master=frame_e, text="Total Bill")
bill_lbl.grid(row=11, column=0, sticky="we")

empty_space = tk.Label(master=frame_e, text="").grid(row=12, coloumn=0, sticky="w")

btn_submit = tk.Button(master=frame_e, text="Submit", command=place_booking)
btn_submit.grid(row=13, coloumn=1, sticky="ew")
 
btn_reset = tk.Button(master=frame_e, text="Reset", command=reset_booking)
btn_reset.grid(row=13, column=2, sticky="ew")

btn_close = tk.Button(master=frame_e, text="Close", command=window.destroy)
btn_close.grid(row=14, column=1, sticky="ew")

 #compiling
frame_e.grid(row=0, couloumn=0, padx=10)

#Running the application
window.mainloop()
